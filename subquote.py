#! /usr/bin/python3

# A program that gives you a random quote from an srt (subtitles) file

import random
import os
import re

print('This Program gives you a random quote from a subtitles file.\nPlease enter the path of the srt file!')

whichfile=input()

file=open(whichfile)
spam=file.read()

bacon=re.compile(r'([a-zA-Z].*\s[a-zA-Z].*)')
join=re.compile(r'\n')
joining=re.compile(r',\n')

eggs=bacon.findall(spam)

quotes=open('quotes1.txt','w')

for i in range(len(eggs)):
    blob=join.sub(' ',str(eggs[i]))
    quotes.write(blob)
    quotes.write('\n')

quotes.close()

banana=open('quotes1.txt')
bogus=banana.read()
banana.close()

blobber=joining.sub(', ',bogus)

rama=open('quotes2.txt','w')
rama.write(blobber)
rama.close()

dama=open('quotes2.txt')
nama=dama.readlines()
dama.close()

# uncomment if you want to delete lines at the beginning or end

#del nama[0]

#for i in range(10):
#    del nama[len(nama)-1]

#numa=open('quotes.txt','a')

#for i in range(len(nama)):
#    numa.write(str(nama[i]))

#numa.close()

os.unlink('quotes1.txt')
os.unlink('quotes2.txt')

while True:
    print('-------------------------------------')
    print('')
    print(nama[random.randint(0,len(nama)-1)])
    print('-------------------------------------')
    print('Do you want another quote? [y/n]')
    yn=input()
    if yn=='y':
        continue
    else:
        break